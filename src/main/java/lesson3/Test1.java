package lesson3;

import java.util.Scanner;

public class Test1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите ваше имя");
        String name = input.nextLine();
        boolean isBob = !true;

        System.out.println("Введите ваш возраст");
        int age = input.nextInt();

        System.out.println("Введите сумму кредита");
        int sumCred = input.nextInt();
        String message = (!"Bob".equals(name) && age >= 18 && sumCred <= age*100) ? "Кредит выдан" : "Кредит не выдан";
        System.out.println(message);
    }
}
